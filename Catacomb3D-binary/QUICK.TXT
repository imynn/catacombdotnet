^C^I*                      Notice to SVGA users!                       *

^C^1****************************************
^C^1TO ALL USERS WITH SVGA GRAPHICS ADAPTERS
^C^1****************************************


If your computer has an SVGA graphics card, you may see some unwanted 
graphics effects when you begin to play the actual game.  

There is nothing to worry about if that happens. 

To correct any problems with the graphics display of this game, type 
the following to begin play:

START /COMP


This should clear up any graphics display problems you might see on 
your SVGA system!


For technical support on this program, call 1-318-221-8311.


^C^ISome DOS Command Line parameters you might need to use


START /NOAL          Turn off sound card detection

START /NOJOYS        Turn off joystick detection

START /NOMOUSE       Turn off mouse detection

START /HIDDENCARD    Override video card detection if program seems to
                     be detecting your video card incorrectly and not
                     letting you play.


^C^I*                       System requirements                           *


    Computer type: 
   An XT, 286, 386, or 486 processor.

    Memory: 
   You need 640K of conventional memory with at least 525K free.

    Graphics mode:
   EGA graphics required (supported on EGA, VGA, & SVGA cards).

    Sound support: 
   Ad Lib, Sound Blaster, and PC internal speaker sound.

    Game controls: 
   The game is controlled by use of your keyboard, joystick, or mouse.


^C^I*                             Credits                              *

John Carmack                    Programming
John Romero                     Programming
Tom Hall                        Creative Director
Adrian Carmack                  Art Director
Jim Weiler                      Quality Assurance Director
Judi Mangham                    Quality Assurance Testing


^C^I*                    Warranties and Disclaimers                    *

^C^1LIMITED WARRANTY AND DISCLAIMER

SOFTDISK PUBLISHING WARRANTS THAT THIS SOFTWARE WILL PERFORM SUBSTANTIALLY IN ACCORDANCE WITH ANY WRITTEN GUARANTEES PUBLISHED BY SOFTDISK. 

SOFTDISK PUBLISHING DISCLAIMS ALL OTHER WARRANTIES RELATING TO THIS SOFTWARE, WHETHER EXPRESS, IMPLIED, STATUTORY OR IN ANY COMMUNICATION WITH YOU, AND SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR US

SOME STATES DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU.  THIS WARRANTY GIVES YOU SPECIFIC LEGAL RIGHTS.  YOU MAY ALSO HAVE OTHER RIGHTS WHICH VARY FROM STATE TO STATE.

^C^1LIMITATION OF LIABILITY

IN NO EVENT WILL SOFTDISK PUBLISHING BE LIABLE FOR ANY DAMAGES, INCLUDING LOSS OF DATA, LOST PROFITS, COST OF COVER OR OTHER SPECIAL, INCIDENTAL, CONSEQUENTIAL OR INDIRECT DAMAGES ARISING FROM THE USE OR ATTEMPTED USE OF THIS SOFTWARE OR ACCOMPANYING DOCUM

THIS LIMITATION WILL APPLY EVEN IF SOFTDISK PUBLISHING  HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  UNDER NO CIRCUMSTANCES SHALL THE LIABILITY OF SOFTDISK PUBLISHING EXCEED THE ACTUAL AMOUNT PAID TO AND RECEIVED BY SOFTDISK PUBLISHING IN CONNECTIO

SOME STATES DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.

THIS AGREEMENT SHALL BE GOVERNED BY THE LAWS OF THE STATE OF LOUISIANA WITHOUT REGARD TO THE CHOICE OF LAW RULES OF SUCH STATE.

