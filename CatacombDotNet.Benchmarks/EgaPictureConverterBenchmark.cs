﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using CatacombDotNet.Core.Compression;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Graphics;
using CatacombDotNet.Implementation.Compression.Huffman;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Graphics;
using CatacombDotNet.Variables.Graphics;

namespace CatacombDotNet.Benchmarks
{
    public class EgaPictureConverterBenchmark
    {
        private readonly IChunkHeader _header;
        private readonly IChunkCompInfoGetter _compInfoGetter;
        private readonly IDecompressor _decompressor;
        private readonly string _egaGraphPath;
        private readonly PicTable _picTable;
        private readonly byte[] _decompChunkBuffer;
        private readonly IPictureConverter _pictureConverter;

        public EgaPictureConverterBenchmark()
        {
            _header = new EgaChunkHeader(Ega.Header);
            _compInfoGetter = new EgaChunkCompInfoGetter();
            _decompressor = new HuffmanDecompressor(Ega.HuffmanDict);
            _egaGraphPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(),
                ConfigurationManager.AppSettings.Get("egaGraphPath")));
            _decompChunkBuffer = GetGraphicsChunk((int) Graphicnums.TITLEPIC);
            _picTable = GetPictureTable()[(int)Graphicnums.TITLEPIC - GraphicConstants.STARTPICS];
            var pictureConverter = new EgaPictureConverter(new EgaPlanesColorConverter(), new EgaToRgbColorConverter())
            {
                IsParallel = true
            };
            _pictureConverter = pictureConverter;
        }

        [Benchmark]
        public void EgaToBitmap() => _pictureConverter.ToBitmap(_decompChunkBuffer, 0, _decompChunkBuffer.Length, new Bitmap(_picTable.Width * 8, _picTable.Height));

        private PicTable[] GetPictureTable()
        {
            //считываем picture table
            var chunkPtInfo = _header.GetChunkInfo(GraphicConstants.STRUCTPIC);
            var chunkPtBuffer = new byte[chunkPtInfo.Length];

            using (var reader = new ChunkReader(new FileStream(_egaGraphPath, FileMode.Open,
                FileAccess.Read, FileShare.Read), _header))
            {
                reader.Read(chunkPtInfo.Chunk, chunkPtBuffer, 0);
            }

            chunkPtInfo = _compInfoGetter.GetChunkInfo(chunkPtInfo.Chunk, chunkPtBuffer);

            var decompChunkPtBuffer = new byte[chunkPtInfo.Length];
            _decompressor.Decompress(chunkPtBuffer, chunkPtInfo.Offset, chunkPtBuffer.Length - chunkPtInfo.Offset,
                decompChunkPtBuffer, 0, decompChunkPtBuffer.Length);

            return PicTable.FromByteArray(decompChunkPtBuffer, GraphicConstants.NUMPICS);
        }

        private byte[] GetGraphicsChunk(int chunk)
        {

            var chunkInfo = _header.GetChunkInfo(chunk);
            var chunkBuffer = new byte[chunkInfo.Length];

            using (var reader = new ChunkReader(new FileStream(_egaGraphPath, FileMode.Open,
                FileAccess.Read, FileShare.Read), _header))
            {
                reader.Read(chunkInfo.Chunk, chunkBuffer, 0); 
            }

            chunkInfo = _compInfoGetter.GetChunkInfo(chunk, chunkBuffer);

            var decompChunkBuffer = new byte[chunkInfo.Length];
            _decompressor.Decompress(chunkBuffer, chunkInfo.Offset, chunkBuffer.Length - chunkInfo.Offset,
                decompChunkBuffer, 0, decompChunkBuffer.Length); // распаковка chunk

            return decompChunkBuffer;
        }


    }
}
