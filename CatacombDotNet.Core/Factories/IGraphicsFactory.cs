﻿using CatacombDotNet.Core.Resources.Graphics;
using System.Drawing;

namespace CatacombDotNet.Core.Factories
{
    public interface IGraphicsFactory
    {
        Bitmap GetTile8x8(int chunk);
        MaskedBitmap GetSprite(int chunk);
        Bitmap GetBitmap(int chunk);
        MaskedBitmap GetMaskedBitmap(int chunk);
        string GetText(int chunk);
        Bitmap GetFont(int chunk, int charIndex);
    }
}
