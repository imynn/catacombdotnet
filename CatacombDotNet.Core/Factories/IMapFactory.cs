﻿using CatacombDotNet.Core.Resources.Maps;

namespace CatacombDotNet.Core.Factories
{
    public interface IMapFactory
    {
        MapType GetMap(int map);
    }
}