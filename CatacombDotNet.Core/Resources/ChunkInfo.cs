﻿namespace CatacombDotNet.Core.Resources
{
    public struct ChunkInfo
    {
        public readonly int Chunk;
        public readonly int Offset;
        public readonly int Length;

        public ChunkInfo(int chunk, int offset, int length)
        {
            Chunk = chunk;
            Offset = offset;
            Length = length;
        }
    }
}
