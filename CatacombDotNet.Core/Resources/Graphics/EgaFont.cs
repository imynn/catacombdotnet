﻿using System;

namespace CatacombDotNet.Core.Resources.Graphics
{
    // ID_VW.H -> 186 line
    public sealed class EgaFont
    {
        private readonly byte[] _data;
        public short Height { get; }
        public short[] Location { get; }
        public sbyte[] Width { get; }

        // STARTFONT -> index: 32 - 140
        // STARTFONT + 1 -> index: 32 - 130
        public EgaChar? this[int index]
        {
            get
            {
                var location = Location[index] - 770;
                var nextLocation = Location[index + 1] - 770;

                if (location < 0)
                    return null;

                if (nextLocation < 0)
                    nextLocation = _data.Length;

                var size = nextLocation - location;
                var data = new byte[size];
                Array.Copy(_data, location, data, 0, data.Length);
                return new EgaChar(Height, Width[index], data);
            }
        }

        public EgaFont(byte[] buffer, int offset, int length)
        {
            Height = BitConverter.ToInt16(buffer, offset);
            offset += 2;

            Location = new short[256];

            for (var i = 0; i < 256; i++)
            {
                Location[i] = BitConverter.ToInt16(buffer, offset);
                offset += 2;
            }

            Width = new sbyte[256];

            for (var i = 0; i < 256; i++)
            {
                Width[i] = (sbyte) buffer[offset];
                offset += 1;
            }

            _data = new byte[length - offset];
            Array.Copy(buffer, offset, _data, 0, _data.Length);
        }

    }
}
