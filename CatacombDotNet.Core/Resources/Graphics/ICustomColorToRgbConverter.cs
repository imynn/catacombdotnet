﻿using System.Drawing;

namespace CatacombDotNet.Core.Resources.Graphics
{
    public interface ICustomColorToRgbConverter
    {
        Color ToRgb(int color);
    }
}
