﻿using System.Drawing;

namespace CatacombDotNet.Core.Resources.Graphics
{
    public class MaskedBitmap
    {
        public Bitmap Bitmap { get; }
        public Bitmap Mask { get; }

        public MaskedBitmap(Bitmap bitmap, Bitmap mask)
        {
            Bitmap = bitmap;
            Mask = mask;
        }
    }
}
