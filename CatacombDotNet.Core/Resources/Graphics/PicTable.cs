﻿using System;
using System.Runtime.InteropServices;

namespace CatacombDotNet.Core.Resources.Graphics
{

    //typedef struct
    //{
    //int width, height;
    //} pictabletype;

    /// <summary>
    /// ID_VW.H, line 180
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PicTable
    {
        public short Width;
        public short Height;

        public PicTable(short width, short height)
        {
            Width = width;
            Height = height;
        }

        public static PicTable[] FromByteArray(byte[] buffer, int numpics)
        {
            var picTable = new PicTable[numpics];

            for (int i = 0; i < picTable.Length; i++)
            {
                picTable[i] = new PicTable(BitConverter.ToInt16(buffer, i * 4),
                    BitConverter.ToInt16(buffer, i * 4 + 2));
            }

            return picTable;
        }
    }

}
