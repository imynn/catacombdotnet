﻿namespace CatacombDotNet.Core.Resources
{
    public interface IChunkReader
    {
        int Read(int chunk, byte[] buffer, int offset);
    }
}