﻿using System;

namespace CatacombDotNet.Core.Resources.Maps
{

    //    typedef struct
    //{

    //long planestart[3];
    //unsigned planelength[3];
    //unsigned width, height;
    //char name[16];
    //}
    //maptype;


    /// <summary>
    /// ID_CA.H line 50
    /// </summary>
    public struct MapType
    {
        public readonly int[] PlaneStart;
        public readonly ushort[] PlaneLength;
        public readonly ushort Width;
        public readonly ushort Height;
        public readonly byte[] Name;

        public MapType(byte[] buffer, int offset, int length)
        {
            PlaneStart = new int[3];

            for (var i = 0; i < PlaneStart.Length; i++)
            {
                PlaneStart[i] = BitConverter.ToInt32(buffer, offset);
                offset += 4;
            }

            PlaneLength = new ushort[3];

            for (var i = 0; i < PlaneLength.Length; i++)
            {
                PlaneLength[i] = BitConverter.ToUInt16(buffer, offset);
                offset += 2;
            }

            Width = BitConverter.ToUInt16(buffer, offset);
            offset += 2;

            Height = BitConverter.ToUInt16(buffer, offset);
            offset += 2;

            Name = new byte[16];

            for (var i = 0; i < Name.Length; i++)
            {
                Name[i] = (byte) buffer[offset++];
            }
        }

        /// <summary>
        /// 3*int + 3*ushort + ushort + ushort + 16*byte
        /// </summary>
        public static int SizeOf => 3 * sizeof(int) + 3 * sizeof(ushort) + sizeof(ushort) + sizeof(ushort) + 16 * sizeof(byte);
    }
}
