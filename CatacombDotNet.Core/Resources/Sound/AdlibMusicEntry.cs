namespace CatacombDotNet.Core.Resources.Sound
{
    public struct AdlibMusicEntry
    {
        public readonly byte Register;
        public readonly byte Data;
        public readonly ushort Delay;

        public AdlibMusicEntry(byte register, byte data, ushort delay)
        {
            Register = register;
            Data = data;
            Delay = delay;
        }
    }
}