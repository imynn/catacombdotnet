﻿using System;

namespace CatacombDotNet.Core.Resources.Sound
{

    // http://www.shikadi.net/moddingwiki/AudioT_Format Adlib Sounds
    //    Data type    Name           Description
    //
    //    UINT32LE     length         Length of sound data
    //    UINT16LE     priority       Sound priority (Only 1 sound may play at a time; any sound will interrupt a sound of equal or lower priority.)
    //    BYTE[16]     instrument     Instrument settings
    //    BYTE block   Octave number
    //    BYTE[length] data           Actual sound data
    //    UINT8        terminator     (Muse-only?)
    //    ASCIIZ[]     name           NULL-terminated Instrument name(optional, Muse-only)

    // http://www.shikadi.net/moddingwiki/Adlib_sound_effect Adlib sound effect 
    // 
    // UINT8[16]    instrument     The OPL register settings for the instrument
    // UINT8        octave         Octave to play notes at
    // BYTE[...]    notes          Pitch data 

    // 1. Данные инструмента (instrument) загружаются в регистры для канала #0
    // 2. Значение октавы записывается в регистр 0xB0. Т.к. этот регистр содержит другие важные поля
    // его значение вычисляется по следующим формулам: 

    // block = (octave & 7) << 2
    // regB0 = block | other_fields

    // regB0 = 0x20            // note on, and set octave to zero!
    // regB0 = block | 0x20    // note on
    // regB0 = block           // note off (0x20 not set)

    // Instrument data (Данные инструмента)

    //Data     type     Name OPL register Description
    //UINT8    mChar    0x20 	          Modulator characteristics
    //UINT8    cChar    0x23 	          Carrier characteristics
    //UINT8    mScale   0x40 	          Modulator scale
    //UINT8    cScale   0x43 	          Carrier scale
    //UINT8    mAttack  0x60 	          Modulator attack/decay rate
    //UINT8    cAttack  0x63 	          Carrier attack/decay rate
    //UINT8    mSust    0x80 	          Modulator sustain
    //UINT8    cSust    0x83 	          Carrier sustain
    //UINT8    mWave    0xE0 	          Modulator waveform
    //UINT8    cWave    0xE3 	          Carrier waveform
    //UINT8    nConn    0xC0 	          Feedback/connection(usually ignored and set to 0)
    //UINT8    voice    - 	              unknown(Muse-only)
    //UINT8    mode     - 	              unknown(Muse-only)
    //UINT8[3] padding  - 	              Pad instrument definition up to 16 bytes

    // 3. Каждый байт нот (notes) содержит одно значение для записи в регистр 0xA0
    // ноты имеют постоянную частоту - 140 Гц. Если байт ноты равен 0, то ноту следует отключить

    /// <summary>
    /// ID_SD.H
    /// </summary>
    public struct AdlibSoundEntry // ID_SD.C -> line 530 -> SDL_ALPlaySound(AdLibSound far *sound)
    {
        public readonly uint Length; // ID_SD.H -> line 47 -> SoundCommon
        public readonly ushort Priority; // ID_SD.H -> line 47 -> SoundCommon
        public readonly byte[] Instrument;  // ID_SD.H -> line 97 -> Instrument
        public readonly byte Octave; // ID_SD.H -> line 112 -> AdLibSound
        public readonly byte[] Data; // ID_SD.H -> line 112 -> AdLibSound

        public AdlibSoundEntry(byte[] buffer, int offset, int length)
        {
            Length = BitConverter.ToUInt32(buffer, offset);
            offset += 4;

            Priority = BitConverter.ToUInt16(buffer, offset);
            offset += 2;

            Instrument = new byte[16];
            Array.Copy(buffer, offset, Instrument, 0, Instrument.Length);
            offset += Instrument.Length;

            Octave = buffer[offset];
            offset += 1;

            Data = new byte[Length];
            Array.Copy(buffer, offset, Data, 0, Data.Length);
        }
    }

    // Стандартный формат audio-CD LPCM, 2 канала, частота дискретизации 44100 Гц, 16 бит на семпл
    // OPL2 (YM3812) 2-operator mono FM Synthesis
}
