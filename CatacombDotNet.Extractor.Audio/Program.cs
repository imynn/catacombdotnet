﻿using System;
using System.Configuration;
using System.IO;
using CatacombDotNet.Implementation.Factories.Sound;
using CatacombDotNet.Variables.Sound;

namespace CatacombDotNet.Extractor.Audio
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var cDir = Directory.GetCurrentDirectory();

                var audioPath = Path.GetFullPath(Path.Combine(cDir,
                    ConfigurationManager.AppSettings.Get("audioPath")));

                if (!File.Exists(audioPath))
                {
                    throw new FileNotFoundException(@"AUDIO file not found!");
                }

                var extrPath = Path.GetFullPath(Path.Combine(cDir,
                    ConfigurationManager.AppSettings.Get("extrPath")));

                if (!Directory.Exists(extrPath))
                    Directory.CreateDirectory(extrPath);

                var soundFactory = new DefaultSoundFactory(audioPath);

                var sounds = Enum.GetValues(typeof(Soundnames));

                foreach (int sound in sounds)
                {
                    if ((Soundnames) sound == Soundnames.LASTSOUND)
                        continue;

                    var stream = soundFactory.GetSound(SoundConstants.STARTADLIBSOUNDS + sound);
                    File.WriteAllBytes(Path.Combine(extrPath,$"{Enum.GetName(typeof(Soundnames), sound)}.wav"), stream.ToArray());
                }


                var musics = Enum.GetValues(typeof(Musicnames));

                foreach (int music in musics)
                {
                    if ((Musicnames)music == Musicnames.LASTMUSIC)
                        continue;

                    var stream = soundFactory.GetMusic(SoundConstants.STARTMUSIC + music);
                    File.WriteAllBytes(Path.Combine(extrPath, $"{Enum.GetName(typeof(Musicnames), music)}.wav"), stream.ToArray());
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}


