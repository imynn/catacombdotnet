﻿using System;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Graphics;
using CatacombDotNet.Variables.Graphics;

namespace CatacombDotNet.Implementation.Resources.Graphics
{
    public sealed class EgaChunkCompInfoGetter: IChunkCompInfoGetter
    {

        /// <summary>
        /// void CAL_ExpandGrChunk (int chunk, byte far *source), ID_CA.C line 1427
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="compChunk"></param>
        /// <returns></returns>

        public unsafe ChunkInfo GetChunkInfo(int chunk, byte[] compChunk)
        {
            int length;
            int offset = 0;

            if (chunk == GraphicConstants.STRUCTPIC)
            {
                length = GraphicConstants.NUMPICS * sizeof(PicTable);
            }
            else if  (chunk == GraphicConstants.STRUCTPICM)
            {
                length = GraphicConstants.NUMPICM * sizeof(PicTable);
            }
            else if (chunk == GraphicConstants.STRUCTSPRITE)
            {
                length = GraphicConstants.NUMSPRITES * sizeof(SpriteTable);
            }
            else if (chunk >= GraphicConstants.STARTTILE8 && chunk < GraphicConstants.STARTEXTERNS)
            {
                if (chunk < GraphicConstants.STARTTILE8M)
                    length = Ega.BLOCK * GraphicConstants.NUMTILE8;
                else if (chunk < GraphicConstants.STARTTILE16)
	                length = Ega.MASKBLOCK * GraphicConstants.NUMTILE8M;
				else if (chunk < GraphicConstants.STARTTILE16M)
	                length = Ega.BLOCK * 4;
				else if (chunk < GraphicConstants.STARTTILE32)
	                length = Ega.MASKBLOCK * 4;
				else if (chunk < GraphicConstants.STARTSPRITES)
	                length = Ega.BLOCK * 16;
                else
	                length = Ega.MASKBLOCK * 16;
            }
            else
            {
	            length = BitConverter.ToInt32(compChunk, 0);
	            offset += 4;
            }

            // возможно стоит учесть void CAL_CacheSprite (int chunk, byte far *compressed)

			return new ChunkInfo(chunk, offset, length);
        }
    }
}
