﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CatacombDotNet.Core.Resources.Graphics;
using CatacombDotNet.Variables.Graphics;

namespace CatacombDotNet.Implementation.Resources.Graphics
{
    public sealed class EgaPictureConverter : IPictureConverter
    {
        private readonly IPlanesColorConverter _planesConverter;
        private readonly ICustomColorToRgbConverter _rgbConverter;

        public bool IsParallel { get; set; } = true;

        public EgaPictureConverter(IPlanesColorConverter planesColorConverter, ICustomColorToRgbConverter rgbConverter)
        {
            _planesConverter = planesColorConverter;
            _rgbConverter = rgbConverter;
        }
     
        public void ToBitmap(byte[] source, int offset, int length, Bitmap bitmap)
        {

            var linewidth = bitmap.Width / Ega.PIXELS_IN_BYTE;
            var planesize = (length - offset) / 4; 

            var bIndex = 0;
            var gIndex = planesize;
            var rIndex = planesize * 2;
            var iIndex = planesize * 3;

            var rectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            var bitmapData = bitmap
                .LockBits(rectangle, ImageLockMode.ReadWrite, bitmap.PixelFormat);

            if (IsParallel)
            {
                Parallel.For(0, bitmap.Height - 1,
                    y => SetLineBitmap(source, offset, y, linewidth, bitmapData, bIndex, gIndex, rIndex, iIndex));
            }
            else
            {
                for (var y = 0; y < bitmap.Height; y++)
                {
                    SetLineBitmap(source, offset, y, linewidth, bitmapData, bIndex, gIndex, rIndex, iIndex);
                }
            }

            bitmap.UnlockBits(bitmapData);
        }

        public void ToCharBitmap(byte[] source, int offset, int length, Bitmap bitmap)
        {
            var linewidth = bitmap.Width / Ega.PIXELS_IN_BYTE;

            if (bitmap.Width % Ega.PIXELS_IN_BYTE > 0)
                linewidth++;

            var rectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            var bitmapData = bitmap
                .LockBits(rectangle, ImageLockMode.ReadWrite, bitmap.PixelFormat);

            if (IsParallel)
            {
                Parallel.For(0, bitmap.Height - 1,
                    y => SetLineChar(source, offset, y, linewidth, bitmapData));
            }
            else
            {
                for (var y = 0; y < bitmap.Height; y++)
                {
                    SetLineChar(source, offset, y, linewidth, bitmapData);
                }
            }

            bitmap.UnlockBits(bitmapData);
        }

        public void ToMaskedBitmap(byte[] source, int offset, int length, Bitmap bitmap, Bitmap mask)
        {
            var linewidth = mask.Width / Ega.PIXELS_IN_BYTE;
            var planesize = (length - offset) / 5;

            var rectangle = new Rectangle(0, 0, mask.Width, mask.Height);
            var bitmapData = mask
                .LockBits(rectangle, ImageLockMode.ReadWrite, mask.PixelFormat);

            if (IsParallel)
            {
                Parallel.For(0, mask.Height - 1,
                    y => SetLineMask(source, offset, y, linewidth, bitmapData));
            }
            else
            {
                for (var y = 0; y < mask.Height; y++)
                {
                    SetLineMask(source, offset, y, linewidth, bitmapData);
                }
            }

            mask.UnlockBits(bitmapData);

            offset += planesize;

            ToBitmap(source, offset, length, bitmap);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void SetLineBitmap(byte[] source, int offset, int y, int linewidth, BitmapData bitmapData, int bIndex, int gIndex, int rIndex, int iIndex)
        {
            for (var lineIndex = 0; lineIndex < linewidth; lineIndex++)
            {
                var bitMask = Ega.START_BIT_MASK;

                for (var i = 0; i < Ega.PIXELS_IN_BYTE; i++)
                {
                    var x = lineIndex * Ega.PIXELS_IN_BYTE + i;
                    var planeIndex = linewidth * y + lineIndex;

                    var blue = Convert.ToInt32((source[offset + planeIndex + bIndex] & bitMask) != 0);
                    var green = Convert.ToInt32((source[offset + planeIndex + gIndex] & bitMask) != 0);
                    var red = Convert.ToInt32((source[offset + planeIndex + rIndex] & bitMask) != 0);
                    var intensity = Convert.ToInt32((source[offset + planeIndex + iIndex] & bitMask) != 0);
                    var color = _rgbConverter.ToRgb(_planesConverter.ToColor(blue, green, red, intensity));

                    SetPixelUnsafe(bitmapData, x, y, color);

                    bitMask = (byte)(bitMask >> 1);
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void SetLineMask(byte[] source, int offset, int y, int linewidth, BitmapData bitmapData)
        {
            for (var lineIndex = 0; lineIndex < linewidth; lineIndex++)
            {
                var bitMask = Ega.START_BIT_MASK;

                for (var i = 0; i < Ega.PIXELS_IN_BYTE; i++)
                {
                    var x = lineIndex * Ega.PIXELS_IN_BYTE + i;
                    var planeIndex = linewidth * y + lineIndex;

                    var blue = 0;
                    var green = 0;
                    var red = 0;
                    var intensity = 0;

                    if ((source[offset + planeIndex] & bitMask) != 0)
                        blue = green = red = intensity = 1;

                    var color = _rgbConverter.ToRgb(_planesConverter.ToColor(blue, green, red, intensity));

                    SetPixelUnsafe(bitmapData, x, y, color);

                    bitMask = (byte)(bitMask >> 1);
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void SetLineChar(byte[] source, int offset, int y, int linewidth, BitmapData bitmapData)
        {
            for (var lineIndex = 0; lineIndex < linewidth; lineIndex++)
            {
                var bitMask = Ega.START_BIT_MASK;

                for (var i = 0; i < Ega.PIXELS_IN_BYTE; i++)
                {
                    var x = lineIndex * Ega.PIXELS_IN_BYTE + i;

                    if (x >= bitmapData.Width)
                        return;

                    var planeIndex = linewidth * y + lineIndex;

                    var blue = 0;
                    var green = 0;
                    var red = 0;
                    var intensity = 0;

                    if ((source[offset + planeIndex] & bitMask) != 0)
                        blue = green = red = intensity = 1;

                    var color = _rgbConverter.ToRgb(_planesConverter.ToColor(blue, green, red, intensity));

                    SetPixelUnsafe(bitmapData, x, y, color);

                    bitMask = (byte)(bitMask >> 1);
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private unsafe void SetPixelUnsafe(BitmapData data, int x, int y, Color color)
        {
            int stride = data.Stride;
            byte* ptr = (byte*)data.Scan0;
            ptr[(x * 4) + y * stride] = color.B;
            ptr[(x * 4) + y * stride + 1] = color.G;
            ptr[(x * 4) + y * stride + 2] = color.R;
            ptr[(x * 4) + y * stride + 3] = color.A;
        }

    }
}

//Для bitmap.SetPixel(x, y, color);
//
//BenchmarkDotNet=v0.11.4, OS=Windows 10.0.17763.379 (1809/October2018Update/Redstone5)
//Intel Core i5-6600 CPU 3.30GHz(Skylake), 1 CPU, 4 logical and 4 physical cores
//[Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3324.0
//DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3324.0
//|      Method |     Mean |     Error |    StdDev |
//|------------ |---------:|----------:|----------:|
//| EgaToBitmap | 20.83 ms | 0.0614 ms | 0.0574 ms |

//Для SetPixelUnsafe(bitmapData, x, y, color);
//
//BenchmarkDotNet=v0.11.4, OS=Windows 10.0.17763.379 (1809/October2018Update/Redstone5)
//Intel Core i5-6600 CPU 3.30GHz(Skylake), 1 CPU, 4 logical and 4 physical cores
//    [Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3324.0
//DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3324.0
//|      Method |     Mean |     Error |    StdDev |
//|------------ |---------:|----------:|----------:|
//| EgaToBitmap | 2.328 ms | 0.0160 ms | 0.0141 ms |

//Для SetPixelUnsafe(bitmapData, x, y, color) + Parallel.For
//
//BenchmarkDotNet=v0.11.4, OS=Windows 10.0.17763.379 (1809/October2018Update/Redstone5)
//Intel Core i5-6600 CPU 3.30GHz(Skylake), 1 CPU, 4 logical and 4 physical cores
//    [Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3324.0
//DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3324.0
//|      Method |     Mean |    Error |   StdDev |
//|------------ |---------:|---------:|---------:|
//| EgaToBitmap | 710.3 us | 9.060 us | 8.031 us |
