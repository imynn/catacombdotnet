﻿using System.Runtime.CompilerServices;
using CatacombDotNet.Core.Resources.Graphics;

namespace CatacombDotNet.Implementation.Resources.Graphics
{
    // http://www.shikadi.net/moddingwiki/Raw_EGA_data
    
    public sealed class EgaPlanesColorConverter: IPlanesColorConverter
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int ToColor(int blue, int green, int red, int intensity)
        {
            return blue ^ (green << 1) ^ (red << 2) ^ (intensity << 3);
        }
    }
}
