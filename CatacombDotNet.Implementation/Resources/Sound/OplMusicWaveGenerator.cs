﻿using System.IO;
using CatacombDotNet.Core.Resources.Sound;

namespace CatacombDotNet.Implementation.Resources.Sound
{
    public class OplMusicWaveGenerator: OplBaseWaveGenerator
    {
        public OplMusicWaveGenerator(int rate, int ticksPerSecond) : base(rate, ticksPerSecond)
        {
        }

        public OplMusicWaveGenerator(int ticksPerSecond) : base(ticksPerSecond)
        {
        }

        public void GenerateWavMusic(Stream stream, AdlibMusicSequence music)
        {
            Init();
            MusicLoop(music, stream);
            Clean();
        }

        private void MusicLoop(AdlibMusicSequence music, Stream stream)
        {
            var streamWriter = new OplWaveStreamWriter(stream);
            streamWriter.SetSampleProvider(this);

            foreach (var musicValue in music.Values)
            {
                Opl.WriteReg(musicValue.Register, musicValue.Data);

                if (musicValue.Delay == 0) continue;

                var buffer = new byte[SamplesPerTick * 2 * musicValue.Delay];
                Read(buffer, buffer.Length);
                streamWriter.Write(buffer, buffer.Length);
            }
        }
    }
}
