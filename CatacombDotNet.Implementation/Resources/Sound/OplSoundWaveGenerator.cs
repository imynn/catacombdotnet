﻿using System.IO;
using CatacombDotNet.Core.Resources.Sound;

namespace CatacombDotNet.Implementation.Resources.Sound
{
    public class OplSoundWaveGenerator : OplBaseWaveGenerator
    {
        //private readonly int[] _carriers = { 3, 4, 5, 11, 12, 13, 19, 20, 21 };
        //private readonly int[] _modifiers = { 0, 1, 2, 8, 9, 10, 16, 17, 18 };

        public const byte Carrier = 3;
        public const byte Modifier = 0;

        public const int AlChar = 0x20;
        public const int AlScale = 0x40;
        public const int AlAttack = 0x60;
        public const int AlSus = 0x80;
        public const int AlWave = 0xe0;
        public const int AlFreqL = 0xa0;
        public const int AlFreqH = 0xb0;
        public const int AlFeedCon = 0xc0;
        public const int AlEffects = 0xbd;

        public OplSoundWaveGenerator(int rate, int ticksPerSecond) : base(rate, ticksPerSecond)
        {
        }

        public OplSoundWaveGenerator(int ticksPerSecond) : base(ticksPerSecond)
        {
        }

        // SDL_ALSoundService

        public void GenerateWavSound(Stream stream, AdlibSoundEntry sound)
        {
            Init();
            SoundLoop(sound, stream);
            Clean();
        }

        private void SoundLoop(AdlibSoundEntry sound, Stream stream)
        {
            var streamWriter = new OplWaveStreamWriter(stream);
            streamWriter.SetSampleProvider(this);
            var alLengthLeft = sound.Length;
            var alSound = sound.Data;
            var alBlock = ((sound.Octave & 7) << 2) | 0x20;
            var inst = sound.Instrument;

            SetFxInst(inst);

            while (alLengthLeft > 0)
            {
                SoundService(alSound, ref alLengthLeft, ref alBlock);
                var buffer = new byte[SamplesPerTick * 2];
                Read(buffer, buffer.Length);
                streamWriter.Write(buffer, buffer.Length);
            }
        }

        private void SetFxInst(byte[] instrument)
        {
            // var c = (byte)_carriers[0];
            // var m = (byte)_modifiers[0];

            var c = Carrier;
            var m = Modifier;

            Opl.WriteReg(m + AlChar, instrument[0]);
            Opl.WriteReg(m + AlScale, instrument[2]);
            Opl.WriteReg(m + AlAttack, instrument[4]);
            Opl.WriteReg(m + AlSus, instrument[6]);
            Opl.WriteReg(m + AlWave, instrument[8]);
            Opl.WriteReg(c + AlChar, instrument[1]);
            Opl.WriteReg(c + AlScale, instrument[3]);
            Opl.WriteReg(c + AlAttack, instrument[5]);
            Opl.WriteReg(c + AlSus, instrument[7]);
            Opl.WriteReg(c + AlWave, instrument[9]);
        }

        private void SoundService(byte[] data, ref uint alLengthLeft, ref int alBlock)
        {
            var s = data[data.Length - alLengthLeft];

            if (s == 0)
            {
                Opl.WriteReg(AlFreqH + 0, 0);
            }
            else
            {
                Opl.WriteReg(AlFreqL + 0, s);
                Opl.WriteReg(AlFreqH + 0, alBlock);
            }

            alLengthLeft--;
        }
    }
}