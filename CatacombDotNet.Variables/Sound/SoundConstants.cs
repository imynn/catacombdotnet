﻿namespace CatacombDotNet.Variables.Sound
{
    /// <summary>
    /// AUDIOC3D.H
    /// </summary>
    public static class SoundConstants
    {
        public static int NUMSOUNDS = 30;
        public static int NUMSNDCHUNKS = 91;
        public static int STARTPCSOUNDS = 0;
        public static int STARTADLIBSOUNDS = 30;
        public static int STARTDIGISOUNDS = 60;
        public static int STARTMUSIC = 90;
    }
}
