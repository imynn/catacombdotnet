﻿using System;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using CatacombDotNet.Implementation.Factories.Graphics;
using CatacombDotNet.Variables.Graphics;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using SharpDX.Direct3D;
using SharpDX.Windows;
using AlphaMode = SharpDX.Direct2D1.AlphaMode;
using Bitmap = SharpDX.Direct2D1.Bitmap;
using BitmapInterpolationMode = SharpDX.Direct2D1.BitmapInterpolationMode;
using PixelFormat = SharpDX.Direct2D1.PixelFormat;
using System.Windows.Forms;
using CatacombDotNet.Engine;

namespace CatacombDotNet
{
    
    public class Program
    {
        /// <summary>
        /// Окно Windows для вывода изображения
        /// </summary>
        private readonly RenderForm _form;

        /// <summary>
        /// Цепочка буферов для вывода изображения на экран
        /// </summary>
        private readonly SwapChain _swapChain; // http://igrocoder.ru/tiki-read_article.php?articleId=45

        /// <summary>
        /// Объект представления видеокарты
        /// </summary>
        private readonly SharpDX.Direct3D11.Device _device;

        /// <summary>
        /// Вторичный буфер
        /// </summary>
        private readonly RenderTargetView _backBufferView;

        /// <summary>
        /// Конфигурация окна Windows
        /// </summary>
        private readonly DemoConfiguration _config;

        /// <summary>
        /// Область рисования Direct2D
        /// </summary>
        private readonly RenderTarget _renderTarget2D; // Direct2D https://docs.microsoft.com/en-us/windows/desktop/Direct2D/the-direct2d-api

        // Время рендера
        private readonly DemoTime clock = new DemoTime();

        private float _frameAccumulator;
        private int _frameCount;
        private float _frameDelta;
        private float _framePerSecond;
        
        private bool _disposed = false;

        //контент
        private GameEngine _gameEngine;


        public Program(DemoConfiguration config)
        {
            _config = config;

            // создаем форму для вывода изображения
            _form = new RenderForm(_config.Title)
            {
                ClientSize = new System.Drawing.Size(_config.Width, _config.Height),
                AllowUserResizing = false
            };

            // Создаем описание для цепочки буферов
            var desc = new SwapChainDescription()
            {
                BufferCount = 1,
                ModeDescription = new ModeDescription(config.Width, config.Height,
                                        new Rational(60, 1), Format.R8G8B8A8_UNorm),
                IsWindowed = true,
                OutputHandle = _form.Handle,
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput
            };

            // Создаем цепочку буферов и объект видеокарты
            SharpDX.Direct3D11.Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.BgraSupport,
                new[] { SharpDX.Direct3D.FeatureLevel.Level_10_0 }, desc, out _device, out _swapChain);

            // Отписываемся от событий окна Windows
            _swapChain.GetParent<SharpDX.DXGI.Factory>().
                MakeWindowAssociation(_form.Handle, WindowAssociationFlags.None);

            //Создаем RenderTarget Direct2D
            var backBuffer = Texture2D.FromSwapChain<Texture2D>(_swapChain, 0);
            _backBufferView = new RenderTargetView(_device, backBuffer);

            using (var surface = backBuffer.QueryInterface<Surface>())
            {
                _renderTarget2D = new RenderTarget(new SharpDX.Direct2D1.Factory(), surface,
                                                  new RenderTargetProperties(new PixelFormat(Format.Unknown, AlphaMode.Premultiplied)));
            }

            _renderTarget2D.AntialiasMode = AntialiasMode.PerPrimitive;

        }

        [STAThread]
        static void Main(string[] args)
        {
            new Program(new DemoConfiguration())
                .Run();
        }

        /// <summary>
        /// Game loop
        /// </summary>
        public void Run()
        {
            bool isFormClosed = false;

            _form.MouseClick += HandleMouseClick;
            _form.KeyDown += HandleKeyDown;
            _form.KeyUp += HandleKeyUp;
            _form.Closed += (o, args) => { isFormClosed = true; };

            LoadContent();

            clock.Start();
            BeginRun();
            RenderLoop.Run(_form, () =>
            {
                if (isFormClosed)
                {
                    return;
                }

                OnUpdate();
                Render();
            });

            UnloadContent();
            EndRun();
        }

        /// <summary>
        ///   Рендер
        /// </summary>
        private void Render()
        {
            _frameAccumulator += _frameDelta;
            ++_frameCount;
            if (_frameAccumulator >= 1.0f)
            {
                _framePerSecond = _frameCount / _frameAccumulator;

                _form.Text = _config.Title + " - FPS: " + _framePerSecond;
                _frameAccumulator = 0.0f;
                _frameCount = 0;
            }

            BeginDraw();
            Draw(clock);
            EndDraw();
        }

        private void BeginDraw()
        {
            _device.ImmediateContext.Rasterizer.SetViewport(new Viewport(0, 0, _config.Width, _config.Height));
            _device.ImmediateContext.OutputMerger.SetTargets(_backBufferView);
            _renderTarget2D.BeginDraw();
            _renderTarget2D.Clear(null);
        }

        private void Draw(DemoTime time)
        {
            _gameEngine.Draw();
        }

        private void EndDraw()
        {
            _renderTarget2D.EndDraw();
            _swapChain.Present(_config.WaitVerticalBlanking ? 1 : 0, PresentFlags.None);
        }



        /// <summary>
        ///   Quits the sample.
        /// </summary>
        public void Exit()
        {
            _form.Close();
        }

        /// <summary>
        ///   Updates sample state.
        /// </summary>
        private void OnUpdate()
        {
            _frameDelta = (float)clock.Update();
            Update(clock);
        }


        private void LoadContent()
        {
            var cDir = Directory.GetCurrentDirectory();

            var egaGraphPath = Path.GetFullPath(Path.Combine(cDir,
                ConfigurationManager.AppSettings.Get("egaGraphPath")));

            _gameEngine = new GameEngine(new DefaultGraphicsFactory(egaGraphPath), _renderTarget2D);
        }

        private void UnloadContent()
        {

        }


        private void Update(DemoTime time)
        {

        }

        private void BeginRun()
        {

        }

        private void EndRun()
        {

        }

        private void MouseClick(MouseEventArgs e)
        {

        }

        private void KeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Exit();
        }

        private void KeyUp(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
                _gameEngine.Back();

            if (e.KeyCode == Keys.Right)
                _gameEngine.Next();
        }

        /// <summary>
        ///   Handles a mouse click event.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.Windows.Forms.MouseEventArgs" /> instance containing the event data.</param>
        private void HandleMouseClick(object sender, MouseEventArgs e)
        {
            MouseClick(e);
        }

        /// <summary>
        ///   Handles a key down event.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.Windows.Forms.KeyEventArgs" /> instance containing the event data.</param>
        private void HandleKeyDown(object sender, KeyEventArgs e)
        {
            KeyDown(e);
        }

        /// <summary>
        ///   Handles a key up event.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.Windows.Forms.KeyEventArgs" /> instance containing the event data.</param>
        private void HandleKeyUp(object sender, KeyEventArgs e)
        {
            KeyUp(e);
        }

        /// <summary>
        ///   Performs object finalization.
        /// </summary>
        ~Program()
        {
            if (!_disposed)
            {
                Dispose(false);
                _disposed = true;
            }
        }

        /// <summary>
        ///   Disposes of object resources.
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(true);
                _disposed = true;
            }
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///   Disposes of object resources.
        /// </summary>
        /// <param name = "disposeManagedResources">If true, managed resources should be
        ///   disposed of in addition to unmanaged resources.</param>
        protected void Dispose(bool disposeManagedResources)
        {
            if (disposeManagedResources)
            {
                if (_form != null)
                    _form.Dispose();

                if (_gameEngine != null)
                    _gameEngine.Dispose();
            }
        }
    }
}
